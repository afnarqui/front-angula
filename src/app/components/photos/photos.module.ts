import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PhotosComponent } from './photos/photos.component';
import { ListComponent } from './photos/list/list.component';

@NgModule({
  declarations: [PhotosComponent, ListComponent],
  imports: [
    CommonModule
  ],
  exports: [
    PhotosComponent,
    ListComponent
  ]
})
export class PhotosModule { }
