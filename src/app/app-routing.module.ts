import { NgModule } from '@angular/core';

import { Routes, RouterModule } from '@angular/router';
import { AppComponent } from './app.component';
import { PhotosComponent } from './components/photos/photos/photos.component';
import { ListComponent } from './components/photos/photos/list/list.component';


const routes: Routes = [
  { path: 'photos', component: PhotosComponent },
  { path: 'home', component: ListComponent }
  ,{ path: '**', redirectTo: 'home' }
];


@NgModule({
  imports: [
    RouterModule.forRoot( routes )
  ],
  exports: [
    RouterModule
  ]
})
export class AppRoutingModule { }
