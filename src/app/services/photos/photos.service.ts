import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../environments/environment.prod';
import { Photos } from '../../models/photos/photos.models';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class PhotosService {
  private url = environment.url;

  constructor(private http: HttpClient) { }

  /**
   * should of return an list of photos
   */
  get() {
    this.http.get<Photos[]>(`${this.url}/photos`)
    .pipe(
      map( response => response)
    );
  }

  /**
   * should of return an entity photos
   */
  getById(id: string) {
    this.http.get<Photos>(`${this.url}/photos/${id}`)
    .pipe(
      map( response => response)
    );
  }
}
