# front-angular

[![N|Solid](https://res.cloudinary.com/drqk6qzo7/image/upload/v1560738328/front-angular-redux-ngrx_xjsksb.jpg)](https://nodesource.com/products/nsolid)

[![Build Status](https://travis-ci.org/joemccann/dillinger.svg?branch=master)](https://travis-ci.org/joemccann/dillinger)

project in angular and redux. developed in javascript using docker-compose

[trello](https://trello.com/b/zGZVO1CA/front-angular) Trello

And of course front-angular itself is open source with a [public repository][afn]
 on GitHub.
 
## Usage

front-angular requires 

[Docker](https://hub.docker.com/editions/community/docker-ce-desktop-windows)
[Git](https://git-scm.com/downloads)

to run.

Install the dependencies and devDependencies previous
start the server.

## download
```sh
git clone git clone git@gitlab.com:afnarqui/front-angular.git
cd front-angular
docker-compose up --build
```

## create image sql server in docker
```docker
docker-compose up --build
```

## run aplication
```sh
http://localhost:82/
```

## exec contenedor in aws
```sh
docker run --rm -d -p 80:80/tcp afnarqui/frontangular:v1
```

## run aplication in production
```sh
http://3.19.120.22/
```

## commands throughout the project
```
docker run --name volumes -v d:/developement:/developement busybox
docker run --name angular -p 82:82 -it --volumes-from volumes -v d:/developement:/developement ubuntu bin/bash
apt-get update
apt-get install sudo
apt-get install git
apt-get install curl
curl -sL https://deb.nodesource.com/setup_10.x | sudo bash -
sudo apt install nodejs
npm install -g @angular/cli
npm install -g http-server
npm install bootstrap
git init
git remote add origin https://gitlab.com/afnarqui/front-angula.git
git add .
git config --global user.name "Andrés Felipe Naranjo Quintero"
git config --global user.email "afnarqui9@gmail.com"
git commit -m "vertion initial"
git push -u origin master
npm install --save-dev @compodoc/compodoc
ng g m shared/shared --flat
ng g c shared/shared/navbar
ng g s services/photos/photos
ng g m components/photos
ng g c components/photos/photos
ng g c components/photos/photos/list
````

[afn]: <https://gitlab.com/afnarqui/front-angula>

# FrontAngular

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 8.0.3.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).
