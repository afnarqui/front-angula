FROM node:10.16.0

LABEL usuario="andresnaranjo@afnarqui.com"

WORKDIR usr

COPY ./dist/frontAngular/ /usr

RUN npm install -g http-server

EXPOSE 82

CMD http-server -p 82


